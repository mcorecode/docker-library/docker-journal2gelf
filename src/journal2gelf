import json
import socket
import ssl
import sys

facility_names = {
    0: "kern",
    1: "user",
    2: "mail",
    3: "daemon",
    4: "auth",
    5: "syslog",
    6: "lpr",
    7: "news",
    8: "uucp",
    9: "cron",
    10: "authpriv",
    16: "local0",
    17: "local1",
    18: "local2",
    19: "local3",
    20: "local4",
    21: "local5",
    22: "local6",
    23: "local7"
}

def journal_to_gelf(record, ovh_token):
    msg = {'version': '1.0'}
    for key, value in record.items():
        # journalctl's JSON exporter will convert unprintable (incl. newlines)
        # strings into an array of integers. We convert these integers into
        # their ascii representation and concatenate them together
        # to reconstitute the string.
        if type(value) is list:
            try:
                value = ''.join([chr(x) for x in value])
            except TypeError:
                # sometimes it's an array of strings. newline join them
                value = '\n'.join(value)

        if key == '__REALTIME_TIMESTAMP':
            # convert from systemd's format of microseconds expressed as
            # an integer to graylog's float format, eg: "seconds.microseconds"
            msg['timestamp'] = float(value) / (1000 * 1000)
        elif key == 'PRIORITY':
            msg['level'] = int(value)
        elif key == 'SYSLOG_FACILITY':
            msg['_facility'] = facility_names.get(int(value), 'unknown')
        elif key == '_HOSTNAME':
            msg['host'] = value
        elif key == 'MESSAGE':
            if value and len(value.strip()) > 0:
                msg['short_message'] = value
        elif key.startswith('.'):
            continue
        elif key == '__CURSOR':
            continue
        else:
            # prefix additional fields with '_' for graylog.
            msg['_' + key] = value

    if ovh_token:
        msg['_X-OVH-TOKEN'] = ovh_token

    return msg

if __name__ == '__main__':
    import optparse

    opts_parser = optparse.OptionParser()
    opts_parser.add_option('-s', '--server', dest='host', default='localhost',
                           help='Graylog2 server host or IP (default: %default)')
    opts_parser.add_option('-p', '--port', dest='port', default=12201, type='int',
                           help='Graylog2 server port (default: %default)')
    opts_parser.add_option('-x', '--ovh-token', dest='ovh_token', default=None,
                           help='OVH token (default: %default)')
    (opts, args) = opts_parser.parse_args()

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(10)
    wrapped_socket = ssl.wrap_socket(sock, ssl_version=ssl.PROTOCOL_TLSv1_2)
    wrapped_socket.connect((opts.host, opts.port))

    for line in iter(sys.stdin.readline, ''):
        if len(line.strip()) > 0:
            try:
                record = json.loads(line)
                gelf = journal_to_gelf(record, opts.ovh_token)
                if 'short_message' in gelf:
                    payload = json.dumps(gelf).encode('utf-8')

                    sent_len = wrapped_socket.send(payload)
                    if not wrapped_socket.send(b"\0") == 1:
                        raise Exception("Something went wrong whne sending log (zero terminator send failed)")

                    payload_len = len(payload)
                    if not payload_len == sent_len:
                        raise Exception("Something went wrong whne sending log. Send only {} out of {}".format(sent_len, payload_len))
            except Exception as e:
                print("Error sending line to graylog:")
                print(line)
                print(e)
