FROM docker.io/library/python:3

RUN mkdir journal2gelf
COPY ./src/requirements.txt journal2gelf/
RUN cd journal2gelf \
    && pip install --no-cache-dir -r requirements.txt
COPY ./src/journal2gelf journal2gelf/

ENTRYPOINT ["python", "/journal2gelf/journal2gelf"]
